import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import replace from 'rollup-plugin-replace'
import filesize from 'rollup-plugin-filesize'
import uglify from 'rollup-plugin-uglify'
import conditional from 'rollup-plugin-conditional'

const env = process.env.NODE_ENV
const isProduction = env === 'production'

export default {
  entry: 'src/main.js',
  format: 'iife',
  dest: 'build/main.bundle.js',
  plugins: [
    babel({
      exclude: 'node_modules/**' // only transpile our source code
    }),
    resolve({
      jsnext: true,
      browser: true
    }),
    commonjs({
      namedExports: {
        'node_modules/inferno-redux/index.js': ['Provider', 'connect'],
        'node_modules/inferno/index.js': ['linkEvent'],
        'node_modules/aphrodite/no-important.js': ['css'],
        'node_modules/aphrodite/lib/index.js': ['StyleSheet']
      }
    }),
    replace({
      'process.env.NODE_ENV': env
    }),
    filesize(),
    conditional(isProduction, [
      uglify()
    ])
  ]
}
