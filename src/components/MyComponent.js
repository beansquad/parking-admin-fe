import Inferno, { linkEvent } from 'inferno'
import Component from 'inferno-component'
import { css } from 'aphrodite/no-important'
import styles from './MyComponent.styles.js'

export default class MyComponent extends Component {

  changeName(dispatch, event) {
    dispatch({
      type: 'CHANGE_NAME',
      name: event.target.value
    })
  }

  render() {
    let store = this.context.store
    let state = store.getState()

    return (
      <div>
        <div className={css(styles.Bar)}>
          <h1 className={css(styles.H1)}>
            {state.name}
          </h1>
        </div>
        <input
          className={css(styles.Input)}
          onInput={linkEvent(store.dispatch, this.changeName)}
          type='text'
        />
      </div>
    )
  }
}