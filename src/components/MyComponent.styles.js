import { StyleSheet } from 'aphrodite'

export default StyleSheet.create({
  H1 : {
    fontFamily: 'Helvetica',
    fontSize: '14px',
    color: 'white',
    textTransform: 'uppercase',
    fontWeight: '700',
    letterSpacing: '2px',
    textAlign: 'center',
    margin: '0'
  },
  Bar: {
      height: '50px',
      width: '100%',
      background: 'linear-gradient(180deg, #1948a1 0%, #0b2b6d 100%)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'relative'
  },
  Input: {
    width: '100%',
    height: '25px',
    marginTop: '5px',
    padding: '0 10px',
    border: '1px solid grey',
    borderRadius: '2px',
    boxSizing: 'border-box',
    color: 'black',
    fontSize: '14px',
    ':focus': {
      borderColor: 'grey',
      outline: 'none'
    },
    ':disabled': {
      border: '0',
      paddingLeft: '0',
      color: 'black',
      opacity: 1,
      background: 'transparent',
      borderRadius: 0
    }
  }
})