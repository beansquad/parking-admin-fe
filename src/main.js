import Inferno from 'inferno'
import { Provider, connect } from 'inferno-redux';
import { createStore } from 'redux'
import MyComponent from './components/MyComponent'

const store = createStore(function(state, action) {
  switch (action.type) {
    case 'CHANGE_NAME':
      return {
        name: action.name
      }
    default:
      return {
        name: 'Olly'
      }
  }
})

function mapStateToProps(state) {
  return {
    name: state.name
  }
}

const App = connect(mapStateToProps)(MyComponent)

const container = document.getElementById('container')

Inferno.render(
  <Provider store={ store }>
    <App />
  </Provider>
, container)